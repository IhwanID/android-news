package id.ihwan.gitsnews.feature.about

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import id.ihwan.gitsnews.R

class AboutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
    }
}
