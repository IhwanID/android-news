package id.ihwan.gitsnews.feature.profile

data class Profile(
    val name: String = "Ihwan Dede",
    val email: String = "codewithihwan@gmail.com",
    val phone: String = "6289693626163",
    val github: String = "https://github.com/ihwanid/"
)